package com.blog.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;


@Tag("UserTest")
@DisplayName("Classe de test login et mot de passe d'user")

class UserTest {
	String []db = {"test1@gmail.com", "test2@gmail.com", "test@gmail.com"};

	@BeforeAll
	static void avantTousLesTests() throws Exception {
		System.out.println("Debut des tests");
	}

	@AfterAll
	static void finDeTousLesTests() throws Exception {
		System.out.println("Fin des tests");
	}
	
	@Test
	@DisplayName("Test sur le login")
	void LoginNameExist() {

		User user  = new User();			
		user.setLogin("test@gmail.com");
		boolean result = user.CheckLoginName(user.getLogin(), db);
		assertTrue(result);
	}
	
	@Test
	@DisplayName("Test sur le login (login n'est par correct ")
	void LoginNameNotExist() {

		User user  = new User();			
		user.setLogin("test123@gmail.com");
		boolean result = user.CheckLoginName(user.getLogin(), db);
		assertFalse(result);

	}
	
	@Test
	@DisplayName("Test sur le login (login est vide ")
	void LoginNameNotEmpty() {

		User user  = new User();
		user.setLogin("");
		boolean result = user.CheckLoginName(user.getLogin(), db);
		assertFalse(result);
	}
	@Test
	@DisplayName("Test sur le mot de passe")
	void IsPasswordEgal() {

		User user  = new User();			
		user.setPassword("azerty");

		assertEquals("azerty", user.getPassword());
	}
	@Test
	@DisplayName("Test sur le mot de passe (les mots de passe ne sont pas les mêmes)")
	void IsPasswordNotEgal() {

		User user  = new User();			
		user.setPassword("azerty");
		assertNotEquals("qwerty", user.getPassword()); 
	}
	
	@Test
	@DisplayName("Test sur le modification de mot de passe ")
	void ChangePassword() {
		User user  = new User();			
		user.setPassword("azerty");
		String newPassword = "12345";
		user.ChangePassword(newPassword);
		
		assertEquals(newPassword, user.getPassword());
		
	} 
	
	
}
