package com.blog.testing;

public class User {
	private String login;
	private String password;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public boolean CheckLoginName(String login, String[] db ) {
		boolean result = false;
		for (int i = 0; i<db.length;i++ ) {
		if(db[i]== login) {
			result=true;
			break;
		}
	}
		return result;
}
	public String  ChangePassword(String newPassword) {
		if(password != null ) {
			this.password = newPassword;
		}
		
		return password;
	}
}
